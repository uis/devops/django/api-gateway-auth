import logging
import os
import sys

import django
from django.conf import settings
from django.db import DEFAULT_DB_ALIAS
from django.test.runner import DiscoverRunner

DIRNAME = os.path.dirname(os.path.realpath(__file__))

settings.configure(
    DEBUG=False,
    SECRET_KEY="placeholder",
    DATABASES={
        DEFAULT_DB_ALIAS: {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": "/tmp/test.db",
        }
    },
    TIME_ZONE="Europe/London",
    USE_TZ=True,
    INSTALLED_APPS=(
        "django.contrib.contenttypes",
        "django.contrib.auth",
        "apigatewayauth",
        "apigatewayauth.tests.mocks",
    ),
    MIDDLEWARE_CLASSES=(),
    MIDDLEWARE=(),
    TEMPLATES=[],
    # point to our mock permissions spec
    PERMISSIONS_SPECIFICATION_URL=(
        os.path.join(DIRNAME, "apigatewayauth/tests/mocks/permissions_spec.yml")
    ),
)

django.setup()

logging.basicConfig()
test_runner = DiscoverRunner()

failures = test_runner.run_tests(["apigatewayauth"])
if failures:
    sys.exit(1)
