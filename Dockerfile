# Dockerfile only for running tox tests
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.10-slim

WORKDIR /usr/src/app

RUN pip install --no-cache-dir poetry tox
