# Changelog

## 0.0.7

Added:

- Remove the trailing slash from the default expected audience (built from the request
  URI)

## 0.0.6

Added:

- Incoming requests now have their identity token verified as having been issued by the
  API Gateway if the `API_GATEWAY_ENFORCE_ID_TOKEN_VERIFICATION` setting is `True`.

## 0.0.5

Added:

- When authenticated, a non-database backed user object is associated with the request.

## 0.0.4

Added:

- Repackaged using poetry.
- Aligned code style with black and isort by means of pre-commit checks.

## 0.0.3

Added:

- A changelog.
