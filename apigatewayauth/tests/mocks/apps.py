from django.apps import AppConfig


class MockAPIGatewayAuthConfig(AppConfig):
    name = "apigatewayauth.tests.mocks"
    verbose_name = "Mock testing app"
