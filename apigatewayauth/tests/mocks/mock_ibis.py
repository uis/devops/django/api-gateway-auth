from dataclasses import dataclass


@dataclass
class MockIbisGroup:
    groupid: str
