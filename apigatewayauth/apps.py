from django.apps import AppConfig


class APIGatewayAuthConfig(AppConfig):
    name = "apigatewayauth"
    verbose_name = "API Gateway Authentication App"
